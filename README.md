# TrafficSim
Project for Csci 144 Concurrent Yield intersection.
Wesley Leach

Requirements 

In this project, you are required to simulate an intelligent traffic light system with 
* multithreading.
    -5 Threads, one for each direction, one preprocessor, and one processor
* A sensor is placed at each street intersection and communicates sensors in the car when close enough (50-100 meters). 
    - At 50 meters
* A FIFO queue is maintained by the sensor at the intersection to store RTP(Request To Pass) by cars. 
    - Used C++ Built in Queue
* Cars whose request is at the HOL (Head of Line) is granted permission without stoping at the intersection.
    - Although it still requires 3 seconds to pass through the intersection
* Your program should simulate random car arriving on the four directions and keeps running without deadlock, crashing, or unexpected termination.
    - Currently working
