// Wesley Leach
// Csci 144 Project
/*
In this project, you are required to simulate an intelligent traffic light system with multithreading.
A sensor is placed at each street intersection and communicates sensors in the car when close enough
(50-100 meters). A FIFO queue is maintained by the sensor at the intersection to store RTP
(Request To Pass) by cars. Cars whose request is at the HOL (Head of Line) is granted permission
without stoping at the intersection. Your program should simulate random car arriving on the four
directions and keeps running without deadlock, crashing, or unexpected termination.

*/
#include <iostream>
// For threads
#include <thread>
// For a standarized ADT
#include <queue>
#include <string>
// For rand
#include <stdlib.h>
#include <time.h>
// For a locking variable
#include <mutex>
// For sleep
#include <unistd.h>
// For vectors
#include <vector>
using namespace std;

// Global definitions-----------------------------------------
// Car ADT
struct car{
    // Direction of travel
    int direction;
    // Velocity of Vehicle
    int veloctiy;
    // Distance from center of intersection
    int distanceFromCenter;

    car(int inputDirection){
        srand( time(NULL) );
        int speed =rand() %50+1;
        veloctiy = speed;
        speed = rand() % 200+1;
        distanceFromCenter = speed;
        direction = inputDirection;


    }
};
// Max size of simulation Queue
#define QUEUE_SIZE 100
// Maximum Distance from center
#define MAX_DISTANCE 200
// Distance threshold for senor Trigger
#define SENSOR_TRIGGER 50

// Main Queue that will store traffic
queue<car> MainQueue;
vector<car> DistanceArray;
mutex Queue_Lock;

// Global definitions End ------------------------------------

void generateTraffic(int inputNumber){

    // Generate the traffic here
    /*
        This will generate the traffic for the simulator
            1) Uses a random number to assign the direction of said traffic
            2) Then pushes that car with its direction onto the queue

    */
    int directionOfThread=inputNumber;
    srand( time(NULL) );
    while(true){
    // Creates a critical section so that this code will be executed
    // atomically

    Queue_Lock.lock();

    int cars =rand() %2 ;
    // Directions assigned clockwise 1=North, 2= East, 3 = South, 4 = West
    if (MainQueue.size() < QUEUE_SIZE)
        for( int x=0; x< cars; x++){
            if(directionOfThread == 1){
                cout<<"Car moving North has entered the simulation."<<endl;
            }
            else if ( directionOfThread == 2){
                cout<<"Car moving East has entered the simulation."<<endl;
            }
            else if ( directionOfThread == 3){
                cout<<"Car moving South has entered the simulation."<<endl;
            }

            else if ( directionOfThread == 4 ){
                cout<<"Car moving West has entered the simulation."<<endl;
            }
        DistanceArray.push_back(car(directionOfThread));
        }
       Queue_Lock.unlock();
       int rand_Sleep = rand()%3+1;
        sleep(rand_Sleep);

    }
    }

// This function takes care of calculating the distance before the cars
// have entered within the threshold range.
// Note since we are completely disregarding the limits of physical space
// I.E not calculating the distance from yield position using length of cars
// I will be allowing the cars to travel at C once they are told to enter the queue
void preTrafficProcessor(){
    while(true){
        Queue_Lock.lock();
        for(int x=0;x<DistanceArray.size();x++){
            DistanceArray.at(x).distanceFromCenter =DistanceArray.at(x).distanceFromCenter - DistanceArray.at(x).veloctiy;
            // Diagnostice Cout
            //cout<<" The distance of car moving "<<DistanceArray.at(x).direction<<" is "<<DistanceArray.at(x).distanceFromCenter<<endl;
            if(DistanceArray.at(x).distanceFromCenter <= SENSOR_TRIGGER){
            // Add the car to the main queue
            cout<<"Car moving "<<DistanceArray.at(x).direction<<" has entered the main queue"<<endl;
            MainQueue.push(DistanceArray.at(x));
            // Deletes that car from the Distance Queue
            DistanceArray.erase(DistanceArray.begin() + x);
            }
        }
        Queue_Lock.unlock();
    }
}

void trafficSim(){
    //put traffic processing here
    /*
        This will process traffic through the queue
            1) It will simply pop the top part of the queue off
    */
    // Seeds the process, solves threads entering non-safe status.
    srand( time(NULL) );
    int direction = rand()%4+1;
    DistanceArray.push_back(car(direction));
    while( true ){
        // Creates a critical section so that this code will be executed
        // atomically
        Queue_Lock.lock();
         // Grabs a pointer to the front of the queue
        if( !MainQueue.empty() ){
            car temp = MainQueue.front();
            if(temp.direction == 1){
                    cout<<"---Car moving North has passed through the stop sign."<<endl;
                }
                else if ( temp.direction == 2){
                    cout<<"--Car moving East has passed through the stop sign."<<endl;
                }
                else if ( temp.direction == 3){
                    cout<<"--Car moving South has passed through the stop sign."<<endl;
                }
                else if (temp.direction == 4 ){
                    cout<<"--Car moving West has passed through the stop sign."<<endl;
                }
        }

        // To avoid random deadlock of state


        else{
            cout<<"--No cars are waiting at the intersection."<<endl;
        }
        // Sleeps the program for 3 seconds.
        MainQueue.pop();
        Queue_Lock.unlock();
        sleep(3);
        // Pops the front of the queue and allows the traffic processor to continue.

    }
}

int main()
{


    // generate traffic for system.

    cout<<"North Thread Created"<<endl;
    thread trafficThreadNorth ( generateTraffic,1 );
    cout<<"East Thread Created"<<endl;
    thread trafficThreadSouth ( generateTraffic,2 );
    cout<<"South Thread Created"<<endl;
    thread trafficThreadEast  ( generateTraffic,3 );
    cout<<"West Thread Created"<<endl;
    thread trafficThreadWest  ( generateTraffic,4 );

    // Thread to handle distance Calculations
    cout<<"Distance Processor thread created"<<endl;
    thread distanceSimThread (preTrafficProcessor );


    // This thread will simulate traffic
    cout<<"Main Traffic Simulation thread created."<<endl;
    thread simThread( trafficSim );

    cout<<"---------------------------------------------------------------"<<endl;

    // Join the threads before exiting the simulation.
    trafficThreadNorth.join();
    trafficThreadSouth.join();
    trafficThreadEast.join();
    trafficThreadWest.join();
    simThread.join();
    distanceSimThread.join();
    return 0;
}
